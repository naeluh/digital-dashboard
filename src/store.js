import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const state = {
  isPreloading: false,
  site: [],
  info: [],
  isFav: [],
  cards: [],
  basicPages: [],
  favButton: true,
  showNotifications: false,
  transition: "scaleDownUp",
  transitioning_in: false,
  transitioning_out: false,
  loaded: true,
  currProject: {},
  index: null
};

const mutations = {
  REPLACE_QUERYDATA(state, data) {
    state.site = data;
    state.info = data.info;
    state.basicPages = data.basicPages;
    return state;
  },
  SET_PROJECT(state, data) {
    state.currProject = data;
    return state;
  },
  beginPreload(state) {
    state.isPreloading = true;
  },
  endPreload(state) {
    state.isPreloading = false;
  },
  SET_LOADED(state, loaded) {
    state.loaded = loaded || false;
  },
  OPEN_NOTIFICATIONS: function(state) {
    state.showNotifications = true;
  },
  CLOSE_NOTIFICATIONS: function(state) {
    state.showNotifications = false;
  },
  SET_TRANSITIONING_IN(state, transitioning) {
    state.transitioning_in = transitioning;
    return state;
  },
  SET_TRANSITIONING_OUT(state, transitioning) {
    state.transitioning_out = transitioning;
    return state;
  }
};

const actions = {
  beginPreload: ({ commit }) => commit("beginPreload"),
  endPreload: ({ commit }) => commit("endPreload"),
  favButton: function(value) {
    state.favButton = value;
  },
  LOAD_DATA(context, payload) {
    // let url = 'http://localhost/data/wp-json/wp/v2/project'
    let url1 = encodeURI(
      "http://gettylabs.org/wp-dashboard/?rest_route=/wp/v2/project&per_page=100"
    );
    let url2 = encodeURI(
      "http://gettylabs.org/wp-dashboard/?rest_route=/wp/v2/project_list_info&per_page=100"
    );
    let url3 = encodeURI(
      "http://gettylabs.org/wp-dashboard/?rest_route=/wp/v2/pages&per_page=100"
    );
    context.commit("SET_LOADED", false);
    console.log(context, payload.params.list);
    axios
      .all([axios.get(url1), axios.get(url2), axios.get(url3)])
      .then(
        axios.spread(function(response1, response2, response3) {
          let data = response1.data;
          let info = [];
          let data1 = response2.data;
          let data2 = response3.data;
          if (
            !(
              Object.keys(payload.params).length === 0 &&
              payload.params.constructor === Object
            )
          ) {
            if (payload.params.list) {
              data = data.filter(function(project) {
                let list = [];
                if (project.acf.list.length > 0) {
                  for (var i = project.acf.list.length - 1; i >= 0; i--) {
                    if (project.acf.list[i].slug === payload.params.list) {
                      list.push(project.acf.list[i].slug);
                    }
                  }
                }
                // Find Projects in lists if there are any
                return list.indexOf(payload.params.list) !== -1;
              });
              info = data1.filter(function(project) {
                let list = [];
                if (project.acf.list.length > 0) {
                  for (var i = project.acf.list.length - 1; i >= 0; i--) {
                    // console.log(project.acf.list[i].slug === payload.params.list)
                    if (project.acf.list[i].slug === payload.params.list) {
                      list.push(project.acf.list[i].slug);
                    }
                  }
                }
                // Find Projects in lists if there are any
                return list.indexOf(payload.params.list) !== -1;
              });
            }
          }
          // console.log(data)
          data = data.filter(function(project) {
            // Find Projects with empty titles
            return project.title.rendered !== "";
          });
          data = data.filter(function(project) {
            // Find Projects with empty descriptions
            return project.content.rendered !== "";
          });
          data = data.filter(function(project) {
            // Find Projects with empty percent completed
            return project.acf.percent_complete !== "";
          });
          // Sort by subtitle
          data.sort(function(a, b) {
            let atitle =
              a.title.rendered.indexOf(":") !== -1
                ? a.title.rendered.split(":")[1]
                : a.title.rendered;
            let btitle =
              b.title.rendered.indexOf(":") !== -1
                ? b.title.rendered.split(":")[1]
                : b.title.rendered;
            if (atitle < btitle) return -1;
            if (atitle > btitle) return 1;
            return 0;
          });
          for (let i = data.length - 1; i >= 0; i--) {
            // Add extra items to object
            let url = data[i].title.rendered.toString().trim();
            url = url
              .toLowerCase()
              .replace(/[^\w\s]/gi, "")
              .replace(/ /g, "-");
            data[i]["url"] = url;
            data[i]["index"] = i;
          }
          for (let i = data2.length - 1; i >= 0; i--) {
            // Add extra items to object
            let url = data2[i].title.rendered.toString().trim();
            url = url
              .toLowerCase()
              .replace(/[^\w\s]/gi, "")
              .replace(/ /g, "-");
            data2[i]["url"] = url;
            data2[i]["index"] = i;
          }
          if (info.length > 0) {
            data["info"] = info[0];
          }
          if (data2.length > 0) {
            data["basicPages"] = data2;
          }
          context.commit("REPLACE_QUERYDATA", data);
          context.commit("SET_LOADED", true);
          // return response.data
        })
      )
      .catch(function(error) {
        return error;
      });
  },
  LOAD_PROJECT: function(context, payload) {
    // console.log(payload)
    context.commit("SET_PROJECT", payload);
  },
  cards: function(value) {
    state.cards = value;
  },
  index: function(value) {
    state.index = value;
  },
  transition: function(value) {
    state.transition = value;
  },
  faved: function(value) {
    let index = state.isFav.indexOf(value);
    if (index === -1) {
      state.isFav.push(value);
    } else {
      state.isFav.splice(index, 1);
    }
  }
};

const getters = {
  loading: state => !state.loaded,
  isPreloading: state => state.isPreloading,
  isfaved: state => state.isFav,
  cards: state => state.cards,
  index: state => state.index,
  favButton: state => state.favButton,
  transition: state => state.transition
};

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
});

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import Footer from "./components/Footer";
import Nav from "./components/Nav";
import Grid from "./components/Grid";
import Detail from "./components/Detail";
import Card from "./components/Card";
import Load from "./components/Load";
import TrimmedCardDesc from "./components/TrimmedCardDesc";
import TrimmedCardTitle from "./components/TrimmedCardTitle";
import MiniD3 from "./components/MiniD3";
import DetailD3 from "./components/DetailD3";
import UpperContent from "./components/UpperContent";
import LowerContent from "./components/LowerContent";
import ShowMore from "./components/ShowMore";
import ProjectTeam from "./components/ProjectTeam";
import DetailContent from "./components/DetailContent";
import Alert from "./components/Alert";
import InfoCard from "./components/InfoCard";
import ProjectInfo from "./components/ProjectInfo";
import RecentActivities from "./components/RecentActivities";
import UpcomingActivities from "./components/UpcomingActivities";
import Timeline from "./components/Timeline";
import store from "./store";
import "./registerServiceWorker";

Vue.component("Nav", Nav);
Vue.component("Footer", Footer);
Vue.component("Grid", Grid);
Vue.component("Detail", Detail);
Vue.component("Card", Card);
Vue.component("Load", Load);
Vue.component("MiniD3", MiniD3);
Vue.component("DetailD3", DetailD3);
Vue.component("UpperContent", UpperContent);
Vue.component("LowerContent", LowerContent);
Vue.component("TrimmedCardDesc", TrimmedCardDesc);
Vue.component("TrimmedCardTitle", TrimmedCardTitle);
Vue.component("ShowMore", ShowMore);
Vue.component("ProjectTeam", ProjectTeam);
Vue.component("DetailContent", DetailContent);
Vue.component("Alert", Alert);
Vue.component("InfoCard", InfoCard);
Vue.component("ProjectInfo", ProjectInfo);
Vue.component("RecentActivities", RecentActivities);
Vue.component("UpcomingActivities", UpcomingActivities);
Vue.component("Timeline", Timeline);

require("./assets/scss/main.scss");
require("../node_modules/normalize.css/normalize.css");

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

import Vue from "vue";
import Router from "vue-router";
import { AsyncDataPlugin } from "vue-async-data-2";
import Grid from "@/components/Grid";
import Detail from "@/components/Detail";
import InfoCard from "@/components/InfoCard";
import PageNotFound from "@/components/PageNotFound";

Vue.use(AsyncDataPlugin);
Vue.use(Router);

export default new Router({
  mode: "history",
  base: "/dashboard",
  routes: [
    {
      path: "/",
      name: "Grid",
      component: Grid
    },
    {
      path: "/home",
      name: "Home",
      component: InfoCard
    },
    {
      path: "/page/:id",
      name: "Page",
      component: InfoCard
    },
    {
      path: "/project/:id",
      name: "Project",
      component: Detail
    },
    {
      path: "/:list/project/:id",
      name: "Project with Argument",
      component: Detail
    },
    {
      path: "/:list/info",
      name: "Info with Argument",
      component: InfoCard
    },
    {
      path: "/:list",
      name: "Grid with Argument",
      component: Grid,
      children: [
        {
          path: "/info",
          name: "Nested Info",
          component: InfoCard
        },
        {
          path: "/project/:id",
          name: "Nested Project",
          component: Detail
        }
      ]
    },
    {
      path: "*",
      component: PageNotFound
    }
  ]
});

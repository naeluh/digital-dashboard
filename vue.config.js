// vue.config.js

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        includePaths: [
          "node_modules/bourbon/app/assets/stylesheets/_bourbon.scss",
          "node_modules/bourbon-neat/core/_neat.scss"
        ]
      }
    }
  }
};
